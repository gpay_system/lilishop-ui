var BASE = {
  /**
   * @description api请求基础路径
   */
  API_DEV: {
    common: "http://127.0.0.1:8890",
    buyer: "http://127.0.0.1:8888",
    seller: "http://127.0.0.1:8889",
    manager: "http://127.0.0.1:8887",
  },
  API_PROD: {
    common: "http://common.youabss.cn",
    buyer: "https://buyer.youabss.cn",
    seller: "http://seller.youabss.cn",
    manager: "http://manager.youabss.cn",
  },
};

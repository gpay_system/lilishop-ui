// 统一请求路径前缀在libs/axios.js中修改
import { getRequest, postRequest, putRequest } from "@/libs/axios";

//查询店铺列表
export const getPageData = params => {
  return getRequest("/store/zft", params);
};

//查询店铺详细
export const getShopDetailData = (id, params) => {
  return getRequest(`/store/zft/store/detail/${id}`, params);
};
export const getShopListData = params => {
  return getRequest(`/store/zft/storeList`, params);
};

//增加店铺列表
export const save = params => {
  return postRequest(`/store/zft/add`, params);
};

//修改店铺列表
export const update = (id, params) => {
  return putRequest(`/store/zft/edit/${id}`, params);
};

//修改结算规则
export const updateSettleRule = (id, params) => {
  return putRequest(`/store/zft/editSettleRule/${id}`, params);
};

//查询店铺详情
export const shopDetail = id => {
  return getRequest(`/store/zft/store/detail/${id}`);
};

export const detail = id => {
  return getRequest(`/store/zft/detail/${id}`);
};

// 获取结算单分页
export const getBuyBillPage = params => {
  return getRequest(`/order/bill/getByPage`, params);
};

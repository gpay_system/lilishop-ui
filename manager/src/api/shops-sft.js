// 统一请求路径前缀在libs/axios.js中修改
import { getRequest, postRequest, putRequest } from "@/libs/axios";

//查询店铺列表
export const getPageData = params => {
  return getRequest("/store/sft", params);
};

//查询店铺详细
export const getShopDetailData = (id, params) => {
  return getRequest(`/store/sft/store/detail/${id}`, params);
};
export const getShopListData = params => {
  return getRequest(`/store/sft/storeList`, params);
};

//增加店铺列表
export const save = params => {
  return postRequest(`/store/sft/add`, params);
};

//修改店铺列表
export const update = (id, params) => {
  return putRequest(`/store/sft/edit/${id}`, params);
};

//修改结算规则
export const updateSettleRule = (id, params) => {
  return putRequest(`/store/sft/editSettleRule/${id}`, params);
};

//查询店铺详情
export const shopDetail = id => {
  return getRequest(`/store/sft/store/detail/${id}`);
};

export const detail = id => {
  return getRequest(`/store/sft/detail/${id}`);
};

export const provinces = () => {
  return getRequest(`/store/sft/getAreaProvinces`);
};

export const cities = provinceCode => {
  return getRequest(`/store/sft/getCities`, { provinceCode });
};

export const personalBanks = () => {
  return getRequest(`/store/sft/getPersonalBanks`);
};

export const corporateBanks = () => {
  return getRequest(`/store/sft/getCorporateBanks`);
};

export const branches = (bankAliasCode, cityCode) => {
  return getRequest(`/store/sft/getBranches`, { bankAliasCode, cityCode });
};
export const searchBank = accountNumber => {
  return getRequest(`/store/sft/searchBank`, { accountNumber });
};
export const upload = url => {
  return postRequest(`/store/sft/upload`, { url });
};

// 获取结算单分页
export const getBuyBillPage = params => {
  return getRequest(`/order/bill/getByPage`, params);
};

var BASE = {
  /**
   * @description api请求基础路径
   */
  API_DEV: {
    common: "http://127.0.0.1:8890",
    buyer: "http://127.0.0.1:8888",
    seller: "http://127.0.0.1:8889",
    manager: "http://127.0.0.1:8887",
    //  manager: "http://192.168.0.120:8887",
    //  common: "http://192.168.0.120:8890",
  },
  API_PROD: {
    common: "http://common.youabss.cn",
    buyer: "https://buyer.youabss.cn",
    seller: "http://seller.youabss.cn",
    manager: "http://manager.youabss.cn",
  },
  /**
   * @description // 跳转买家端地址 pc端
   */
  PC_URL: "http://www.youabss.cn",
  /**
   * @description  // 跳转买家端地址 wap端
   */
  WAP_URL: "http://h5.youabss.cn",
  /**
   *  @description api请求基础路径前缀
   */
  PREFIX: "/manager",
};
